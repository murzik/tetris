# A simple Tetris implementation for the [Arduino Uno Rev3](https://store.arduino.cc/arduino-uno-rev3) 

## Features

- Plays the music you would expect from any decent Tretis clone by using a timer [ISR](src/tetris/music.cpp#L61) "optimized" for speed
- Displays your current score 
- Invisible levels

See the [report (PDF)](https://gitlab.com/murzik/tetris/raw/master/doc/Projektbericht.pdf) or [slides (PDF)](https://gitlab.com/murzik/tetris/raw/master/doc/Praesentation.pdf) for additional information in German.

## Hardware Components
![](https://gitlab.com/murzik/tetris/raw/master/doc/breadboard_view_presentation.svg)

1. [Arduino Uno Rev3](https://store.arduino.cc/arduino-uno-rev3) with a [ATmega328P](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf) and a [RGB LED Matrix Shield](http://anleitung.joy-it.net/wp-content/uploads/2016/09/ARD-RGBShield-Anleitung.pdf)
2. Piezo element
3. Potentiometer for adjusting the volume
4. Seven-segment display [SH546AS or YSD-439AK2B-35](https://www.sparkfun.com/datasheets/Components/LED/7-Segment/YSD-439AK2B-35.pdf)
5. A voltage divider with four buttons (from left to right):
   - Rotate 90 degrees counter clockwise   
   - Move current element left
   - Move current element right
   - Rotate 90 degrees clockwise

## Files
```
.
├── LICENSE
├── doc
│   ├── Praesentation.pdf
│   ├── Projektbericht.pdf
│   └── breadboard_view_presentation.svg
└── src
    ├── notes-parser
    │   ├── parser.py                    // creates C arrays (frequencies and relative durations) from
    │   │                                //   a custom music representation similar to CSV
    │   ├── part1.txt                    // the first part of the music in said notation
    │   └── part2.txt
    └── tetris
        ├── digital_pin_functions.cpp    // a fast implementation of digitalWrite und pinMode
        ├── digital_pin_functions.h
        ├── music.cpp                    // registers a timer ISR to play the music
        ├── music.h
        ├── segment_display.cpp          // seven-segment display library
        ├── segment_display.h
        └── tetris.ino                   // game loop and tetris logic
```